import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';

@Component({
  selector: 'app-starter',
  templateUrl: './starter.component.html',
  styleUrls: ['./starter.component.scss'],
})
export class StarterComponent implements OnInit {
  constructor(private appService: AppService) {}

  ngOnInit(): void {}

  startGame() {
    if (localStorage.getItem('playerName')) {
      this.appService.gameType.emit(localStorage.getItem('playerName'));
      this.appService.gameType.emit(localStorage.getItem('gameType'));
      this.appService.startGame.emit();
    } else {
      // when no-pre selected options, go with this one
      localStorage.setItem('playerName', 'Player 1');
      localStorage.setItem('gameType', 'medium');
      this.appService.gameType.emit('medium');
      this.appService.startGame.emit();
    }
  }
}
