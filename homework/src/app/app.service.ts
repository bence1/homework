import { EventEmitter, Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AppService {
  startGame = new EventEmitter<boolean>();
  gameType = new EventEmitter<string>();
  gameIsOver = new BehaviorSubject<boolean>(false);

  mockHighScores = [
    { name: 'Csodálatos Levente', score: 10 },
    { name: 'Gyönyörű André', score: 10 },
    { name: 'Szuper Gábor', score: 10 },
    { name: 'Szépséges Viktor', score: 10 },
  ];

  constructor() {}

  createRecord(name: string, score: number) {
    this.mockHighScores.push({
      name: name,
      score: score,
    });
  }
}
