import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { Record } from '../record.interface';

@Component({
  selector: 'app-scores',
  templateUrl: './scores.component.html',
  styleUrls: ['./scores.component.scss'],
})
export class ScoresComponent implements OnInit {
  highScores: Array<Record>;

  constructor(private appService: AppService) {}

  ngOnInit(): void {
    this.highScores = this.appService.mockHighScores.sort(function (a, b) {
      return b.score - a.score;
    });
  }
}
