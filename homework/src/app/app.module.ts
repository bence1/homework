import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { StarterComponent } from './starter/starter.component';
import { GameComponent } from './game/game.component';
import { SettingsComponent } from './settings/settings.component';
import { ScoresComponent } from './scores/scores.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HolesComponent } from './game/holes/holes.component';
import { HoleComponent } from './game/holes/hole/hole.component';
import { AppService } from './app.service';

@NgModule({
  declarations: [
    AppComponent,
    StarterComponent,
    GameComponent,
    SettingsComponent,
    ScoresComponent,
    HolesComponent,
    HoleComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, ReactiveFormsModule],
  providers: [AppService],
  bootstrap: [AppComponent],
})
export class AppModule {}
