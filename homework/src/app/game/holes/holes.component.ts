import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-holes',
  templateUrl: './holes.component.html',
  styleUrls: ['./holes.component.scss'],
})
export class HolesComponent implements OnInit {
  indexOfHoles: Array<number>;
  lastHoleNumber: number;
  randomHole: number;
  foundMole = 0;
  playerName = localStorage.getItem('playerName');
  gameType = localStorage.getItem('gameType');
  bestResult = localStorage.getItem('bestResult');
  timeIsUp: boolean;
  timeInSeconds = 10;

  constructor(private appService: AppService, private router: Router) {}

  ngOnInit(): void {
    this.indexOfHoles = [1, 2, 3, 4, 5];
    this.gameStart();
  }

  randomTime(min, max): number {
    return Math.round(Math.random() * (max - min) + min);
  }

  selectHole(): number {
    const hole = Math.floor(Math.random() * this.indexOfHoles.length + 1);

    if (hole === this.lastHoleNumber) {
      return this.selectHole();
    }
    this.lastHoleNumber = hole;
    return hole;
  }

  gameStart(): void {
    this.timeIsUp = false;
    this.takeAction();

    let timer = setInterval(() => {
      if (this.timeInSeconds >= 1) {
        this.timeInSeconds--;
      } else {
        this.timeIsUp = true;
        this.appService.gameIsOver.next(true);
        this.appService.createRecord(
          localStorage.getItem('playerName'),
          this.foundMole
        );
        clearInterval(timer);
      }
    }, 1000);
  }

  setGameType(): number {
    if (this.gameType === 'easy') {
      return this.randomTime(1000, 1500);
    } else if (this.gameType === 'medium') {
      return this.randomTime(500, 1000);
    } else {
      return this.randomTime(100, 500);
    }
  }

  takeAction(): void {
    if (!this.timeIsUp) {
      const randomTime = this.setGameType();
      const randomHole = this.selectHole();

      this.randomHole = randomHole;
      setTimeout(() => {
        this.randomHole = null;
        this.takeAction();
      }, randomTime);
    }
  }

  moleCounter(): void {
    this.foundMole++;
    if (
      Number(localStorage.getItem('bestResult')) < this.foundMole ||
      !localStorage.getItem('bestResult')
    ) {
      localStorage.setItem('bestResult', this.foundMole.toString());
    }
  }

  restartGame() {
    this.foundMole = 0;
    this.timeInSeconds = 10;
    this.timeIsUp = false;
    this.gameStart();
  }

  exitGame() {
    this.router.navigate(['/']);
  }
}
