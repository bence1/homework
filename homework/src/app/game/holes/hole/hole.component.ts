import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';

@Component({
  selector: 'app-hole',
  templateUrl: './hole.component.html',
  styleUrls: ['./hole.component.scss'],
})
export class HoleComponent implements OnInit {
  @Input() hole: number;
  @Input() currentHole: number;
  @Output() foundMole = new EventEmitter<number>();
  @ViewChild('holeElement', { static: false }) holeElement: ElementRef;

  constructor() {}

  ngOnInit(): void {}

  clickHole(hole) {
    if (hole === this.currentHole) {
      this.currentHole = null;
      this.foundMole.emit();
    }
  }

  changeCursorToDown() {
    this.holeElement.nativeElement.style.cursor =
      'url("../../../../assets/hammer_rs.png"), pointer';
  }

  changeCursorToUp() {
    this.holeElement.nativeElement.style.cursor =
      'url("../../../../assets/hammer_rs_90.png"), pointer';
  }
}
