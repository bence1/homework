import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements OnInit {
  profileForm = new FormGroup({
    name: new FormControl('Player 1'),
    difficulty: new FormControl('medium'),
  });

  constructor() {}

  ngOnInit(): void {}

  saveSettings() {
    localStorage.setItem('playerName', this.profileForm.controls.name.value);
    localStorage.setItem(
      'gameType',
      this.profileForm.controls.difficulty.value
    );
  }
}
