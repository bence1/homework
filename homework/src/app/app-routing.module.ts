import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GameComponent } from './game/game.component';
import { ScoresComponent } from './scores/scores.component';
import { SettingsComponent } from './settings/settings.component';
import { StarterComponent } from './starter/starter.component';

const routes: Routes = [
  { path: '', component: StarterComponent },
  { path: 'game', component: GameComponent },
  { path: 'settings', component: SettingsComponent },
  { path: 'scores', component: ScoresComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [],
})
export class AppRoutingModule {}
